require 'rantly'
require 'rantly/rspec_extensions'

require_relative '../badges'

describe Camaloon::Badges do
  before :each do
    @b = Camaloon::Badges.new
  end

  it 'Bades#calculate_price returns the appropriate price for a given quantity' do
    expect(@b.calculate_price(2)).to eq 6
    expect(@b.calculate_price(10)).to eq 10
    expect(@b.calculate_price(25)).to eq 20
    expect(@b.calculate_price(100)).to eq 51
    expect(@b.calculate_price(1000)).to eq 387.6
    expect(@b.calculate_price(5000)).to eq 1622.5
  end

  it 'Badges#calculate_price will multiply it by 3 if 10 > ORDER > 0' do
    property_of { range(1, 9) }.check { |num| expect(@b.calculate_price(num)).to eq(num * 3) }
  end

  it 'Badges#calculate_price will multiply it by 1 if 25 > ORDER > 9' do
    property_of { range(10, 24) }.check { |num| expect(@b.calculate_price(num)).to eq(num * 1) }
  end

  it 'Badges#calculate_price will multiply it by 0.8 if 100 > ORDER > 24' do
    property_of { range(25, 99) }.check { |num| expect(@b.calculate_price(num)).to eq(num * 0.8) }
  end

  it 'Badges#calculate_price will multiply it by 0.51 if 1000 > ORDER > 99' do
    property_of { range(100, 999) }.check(500) { |num| expect(@b.calculate_price(num)).to eq(num * 0.51) }
  end

  it 'Badges#calculate_price will multiply it by 0.3876 if 5000 > ORDER > 999' do
    property_of { range(1000, 4999) }.check(1000) { |num| expect(@b.calculate_price(num)).to eq(num * 0.3876) }
  end

  it 'Badges#calculate_price will multiply it by 0.3245 if ORDER > 4999' do
    property_of { range(5000) }.check(1000) { |num| expect(@b.calculate_price(num)).to eq(num * 0.3245) }
  end
end
