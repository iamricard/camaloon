module Camaloon
  class Badges
    BUNDLES = {
      '5000' => 0.3245,
      '1000' => 0.3876,
      '100' => 0.51,
      '25' => 0.8,
      '10' => 1,
      '1' => 3
    }.freeze

    def calculate_price(quantity)
      quantity * BUNDLES.find { |qty, _| qty.to_i <= quantity }.last
    end
  end
end
