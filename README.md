Badges prices
=============

Running
-------

```sh
$ bundle
$ bundle exec rspec
```

Description
-----------

You're working for a Badges company called "Fancy Badges S.A.". This company is in the business of manufacturing and selling badges to customers, but given the implicit nature of the badges, this product doesn't have one static price (for example, 1 euro per badge) but instead has a pricing model that scales by quantity. Meaning, the badges are cheaper as you purchase more. This fact allows the company to have a pricing model that fits the business customers needs and also the individual customer needs.

The pricing for a badge is described like this:

```
1 -> 3
10 -> 1
25 -> 0.8
100 -> 0.51
1000 -> 0.3876
5000 -> 0.3245
```

This table describes the unitary price (in euros) per each quantity range. This means for example that if you purchase 1 badge, the price is 3 euros per badge. If you purchase 10 badges, the price is 1 euro per badge (and the total price in this case is 10 x 1 = 10 euros.). The unitary price for any quantity higher than the max range given is also the unitary price of the max range given (for 1 million units of badges the unitary price is still 0.3245).

Additionally, the unitary price for any quantity not explicitly given in the table is calculated using a linear interpolation between the two closest quantity ranges defined in the table. For example, for 11 badges the unitary price will be something like 0.995 (approximated), gradually going to 0.8 euros per badge at the quantity of 25.

Your goal is to create a Ruby class that implements a method called calculate_price(quantity) that takes only one argument, the quantity for which the price is requested. The return value of that method should be the total price of the product for that quantity.

For any specification not explicitly stated in this description, feel free to implement it as you believe is the best option.
